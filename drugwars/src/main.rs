///
/// TODO: fix irc colors (one bytes if followed by !number)
/// TODO: more colors
/// TODO: better save system, no need to save everything
/// TODO: panic free!
/// TODO: print rumors at destination
/// TODO: Gotta fix that bug where you can ship multiple times @ a location while exceding capacity
///

pub mod api;
pub mod config;
pub mod dealer;
pub mod definitions;
pub mod drug_wars;
pub mod error;
pub mod render;
pub mod renderer;
pub mod save;
pub mod utils;
pub mod admin;

use std::{
    sync::{Arc, RwLock},
    time::Duration,
};

use definitions::{Drug, Item};
use drug_wars::DrugWars;
use irc::{typemap::TypeMapKey, Irc, IrcPrefix};
use utils::{get_system_output, DealerComponent, Matchable};

struct GameManager;

impl TypeMapKey for GameManager {
    type Value = Arc<RwLock<DrugWars>>;
}

fn main() {
    let drug_wars_arc = Arc::new(RwLock::new(DrugWars::load_config("drugwars_config.yaml")));

    {
        let mut drug_wars = drug_wars_arc.write().unwrap();
        drug_wars.init();
    }

    let mut irc = Irc::from_config("irc_config.yaml")
        .add_resource::<Arc<RwLock<DrugWars>>, GameManager>(drug_wars_arc.clone())
        .add_default_system(melp)
        .add_system("register", register)
        .add_system("melp?", explodes)
        .add_system("m", show_market)
        .add_system("i", show_info)
        .add_system("p", show_people)
        .add_system("t", show_date_time)
        .add_system("h", show_all_commands)
        .add_system("leaderboard", leaderboard)
        .add_system("l", loot)
        .add_system("lm", launder)
        .add_system("heal", heal)
        .add_system("ha", show_admin_commands)
        .add_system("gm", give_money)
        .add_system("gd", give::<Drug>)
        .add_system("gi", give::<Item>)
        .add_system("bd", buy::<Drug>)
        .add_system("sd", sell::<Drug>)
        .add_system("bi", buy::<Item>)
        .add_system("bc", buy_capacity)
        .add_system("si", sell::<Item>)
        .add_system("cc", check_capacity_price)
        .add_system("cf", check_flight_prices)
        .add_system("cshd", check_shipping_prices::<Drug>)
        .add_system("cshi", check_shipping_prices::<Item>)
        .add_system("shd", ship::<Drug>)
        .add_system("shi", ship::<Item>)
        .add_system("f", flight)
        .add_system("a", attack)
        .add_admin_system("dealers", show_all_dealers)
        .add_admin_system("dealer", admin_dealer)
        .add_admin_system("ff", fast_forward)
        .add_admin_system("save", save_game)
        .build();

    irc.connect().unwrap();
    irc.register();

    loop {
        {
            let mut drug_wars = drug_wars_arc.write().unwrap();
            drug_wars.check_new_day(&mut irc);
        }
        irc.update();
        std::thread::sleep(Duration::from_millis(50));
    }
}

fn register(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.register_dealer(prefix.nick))
}

fn show_market(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_market(prefix.nick))
}

fn show_info(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_info(prefix.nick))
}

fn show_people(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_people(prefix.nick))
}

fn show_date_time(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_date_time())
}

fn show_all_commands(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_all_commands())
}

fn leaderboard(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.leaderboard())
}

fn show_admin_commands(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_admin_commands())
}

fn melp(_irc: &mut Irc, _prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    vec!["melp?".to_owned()]
}

fn explodes(_irc: &mut Irc, _prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    vec!["explodes.".to_owned()]
}

fn ship<T: DealerComponent + Matchable>(
    irc: &mut Irc,
    prefix: &IrcPrefix,
    arguments: Vec<&str>,
) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 3 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.ship::<T>(prefix.nick, arguments[0], arguments[1], arguments[2]),
    )
}

fn sell<T: DealerComponent + Matchable>(
    irc: &mut Irc,
    prefix: &IrcPrefix,
    arguments: Vec<&str>,
) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 2 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.sell::<T>(prefix.nick, arguments[0], arguments[1]),
    )
}

fn buy<T: DealerComponent + Matchable>(
    irc: &mut Irc,
    prefix: &IrcPrefix,
    arguments: Vec<&str>,
) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 2 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.buy::<T>(prefix.nick, arguments[0], arguments[1]),
    )
}

fn check_flight_prices(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.check_flight_prices(prefix.nick))
}

fn flight(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 1 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(prefix.nick, drug_wars.fly_to(prefix.nick, arguments[0]))
}

fn give_money(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 2 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }

    get_system_output(
        prefix.nick,
        drug_wars.give_money(prefix.nick, arguments[1], arguments[0]),
    )
}

fn give<T: DealerComponent + Matchable>(
    irc: &mut Irc,
    prefix: &IrcPrefix,
    arguments: Vec<&str>,
) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 3 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.give::<T>(prefix.nick, arguments[1], arguments[2], arguments[0]),
    )
}

fn buy_capacity(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 1 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.buy_capacity(prefix.nick, arguments[0]),
    )
}

fn check_capacity_price(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 1 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.check_capacity_price(prefix.nick, arguments[0]),
    )
}

fn check_shipping_prices<T: DealerComponent + Matchable>(
    irc: &mut Irc,
    prefix: &IrcPrefix,
    arguments: Vec<&str>,
) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 3 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.check_shipping_price::<T>(prefix.nick, arguments[0], arguments[1], arguments[2]),
    )
}

fn show_all_dealers(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let drug_wars = data.read().unwrap();
    get_system_output(prefix.nick, drug_wars.show_all_dealers(prefix.nick))
}

fn admin_dealer(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.admin_dealer(prefix.nick, &arguments))
}

fn fast_forward(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.fast_forward())
}

fn save_game(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.save_game(prefix.nick))
}

fn attack(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 2 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(
        prefix.nick,
        drug_wars.attack(prefix.nick, arguments[0], arguments[1]),
    )
}

fn loot(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 1 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(prefix.nick, drug_wars.loot(prefix.nick, arguments[0]))
}

fn heal(irc: &mut Irc, prefix: &IrcPrefix, _arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    get_system_output(prefix.nick, drug_wars.heal(prefix.nick))
}

fn launder(irc: &mut Irc, prefix: &IrcPrefix, arguments: Vec<&str>) -> Vec<String> {
    let data = irc.data().get::<GameManager>().unwrap();
    let mut drug_wars = data.write().unwrap();
    if arguments.len() != 1 {
        return get_system_output(prefix.nick, drug_wars.show_all_commands());
    }
    get_system_output(prefix.nick, drug_wars.launder(prefix.nick, arguments[0]))
}
