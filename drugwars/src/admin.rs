use std::str::FromStr;

use chrono::NaiveDate;

use crate::{
    definitions::{DealerStatus, Location},
    drug_wars::DrugWars,
    error::{Error, Result},
    utils::{hl_message, pretty_print_money},
};

/*

.dealer info <nick>

 */

impl DrugWars {
    pub fn admin_dealer(&mut self, nick: &str, arguments: &[&str]) -> Result<Vec<String>> {
        if arguments.len() < 1 {
            return self.show_all_commands();
        }

        match arguments[0].to_lowercase().as_str() {
            "info" => {
                let dealer = self.get_dealer(arguments[1])?;
                Ok(self.render_info(arguments[1], dealer))
            }
            "set" => self.admin_set(nick, arguments[1], arguments[2], &arguments[3..]),
            _ => Ok(vec![]),
        }
    }

    fn admin_set(
        &mut self,
        nick: &str,
        dealer_nick: &str,
        cmd: &str,
        args: &[&str],
    ) -> Result<Vec<String>> {
        if args.len() < 1 {
            return Ok(self.render_command_list());
        }

        match cmd {
            "money" => {
                let amount = (self.get_amount_from_str::<f64>(args[0])? * 10000.) as u128;
                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.money = amount;
                Ok(hl_message(
                    nick,
                    &format!("{} has now {}", dealer_nick, pretty_print_money(amount)),
                ))
            }
            "laundermoney" => {
                let amount = (self.get_amount_from_str::<f64>(args[0])? * 10000.) as u128;
                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.laundered_money = amount;
                Ok(hl_message(
                    nick,
                    &format!(
                        "{} has now {} as laundered money",
                        dealer_nick,
                        pretty_print_money(amount)
                    ),
                ))
            }

            "health" => {
                let amount = self.get_amount_from_str::<f32>(args[0])?;
                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.health = amount;
                Ok(hl_message(
                    nick,
                    &format!("{} has now {:.2} hp", dealer_nick, amount),
                ))
            }
            "capacity" => {
                let amount = self.get_amount_from_str::<usize>(args[0])?;
                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.capacity = amount;
                Ok(hl_message(
                    nick,
                    &format!("{} has now {} slots", dealer_nick, amount),
                ))
            }
            "status" => {
                let new_status = match args[0].to_lowercase().as_str() {
                    "available" => DealerStatus::Available,
                    "dead" => {
                        if args.len() < 2 {
                            return Ok(self.render_command_list());
                        }
                        DealerStatus::Dead(match NaiveDate::from_str(args[1]) {
                            Ok(date) => date,
                            Err(_) => return Err(Error::UnknownDate(args[1].to_owned())),
                        })
                    }
                    "flying" => DealerStatus::Flying,
                    _ => return Err(Error::UnknownStatus(args[0].to_string())),
                };

                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.status = new_status;

                Ok(hl_message(
                    nick,
                    &format!("{} has is now {:?}", dealer_nick, dealer.status),
                ))
            }
            "location" => {
                let (new_loc_name, _) = self.get_matching::<Location>(args[0])?;
                let new_loc_name = new_loc_name.clone();

                let mut dealer = self.get_dealer_mut(dealer_nick)?;
                dealer.location = new_loc_name;

                Ok(hl_message(
                    nick,
                    &format!("{} is now at {}", dealer_nick, dealer.location),
                ))
            }
            _ => Ok(vec![]),
        }
    }
}
