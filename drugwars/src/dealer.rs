use std::collections::{HashMap, HashSet};

use itertools::Itertools;
use rand::{seq::IteratorRandom, RngCore};
use serde::{Deserialize, Serialize};

use crate::{
    definitions::{Armor, DealerStatus, Item, ItemKind, OwnedElement},
    drug_wars::DrugWars,
    error::{Error, Result},
    utils::DealerComponent,
};

#[derive(Serialize, Deserialize, Clone)]
pub struct Dealer {
    pub has_attacked: bool,
    pub health: f32,
    pub money: u128,
    pub laundered_money: u128,
    pub location: String,
    pub capacity: usize,
    pub owned_drugs: HashMap<String, HashMap<String, OwnedElement>>,
    pub owned_items: HashMap<String, HashMap<String, OwnedElement>>,
    pub cartel_payroll: u128,
    pub cartel_health: f32,
    pub status: DealerStatus,
    pub looters: HashSet<String>,
}

impl Dealer {
    pub fn random(drug_wars: &DrugWars) -> Self {
        let mut rng = drug_wars.rng.clone();

        let mut owned_drugs = HashMap::default();
        let mut owned_items = HashMap::default();

        for (name, _) in &drug_wars.locations {
            owned_drugs.insert(name.clone(), HashMap::default());
            owned_items.insert(name.clone(), HashMap::default());
        }

        let location_name = drug_wars.locations.keys().choose(&mut rng).unwrap();

        Self {
            has_attacked: false,
            health: 100.,
            money: 1_000 * 10_000,
            laundered_money: 0,
            location: location_name.clone(),
            capacity: 10,
            owned_drugs,
            owned_items,
            cartel_payroll: 0,
            cartel_health: 0.,
            status: DealerStatus::Available,
            looters: HashSet::default(),
        }
    }

    pub fn available(&self) -> bool {
        self.status == DealerStatus::Available
    }

    pub fn is_dead(&self) -> bool {
        match self.status {
            DealerStatus::Dead(_) => true,
            _ => false,
        }
    }

    pub fn get_owned_local<T: DealerComponent>(&self) -> &HashMap<String, OwnedElement> {
        T::get_elements_at(self, &self.location)
    }

    pub fn get_total_owned_local<T: DealerComponent>(&self) -> usize {
        self.get_total_owned_at::<T>(&self.location)
    }

    pub fn get_total_owned_at<T: DealerComponent>(&self, location: &str) -> usize {
        T::get_elements_at(self, &location)
            .iter()
            .map(|(_, e)| e.amount)
            .sum()
    }

    pub fn get_remaining_capacity_local<T: DealerComponent>(&self) -> usize {
        self.get_remaining_capacity_at::<T>(&self.location)
    }

    pub fn get_remaining_capacity_at<T: DealerComponent>(&self, location: &str) -> usize {
        self.capacity - self.get_total_owned_at::<T>(location)
    }

    pub fn print_status(&self) -> &str {
        match self.status {
            DealerStatus::Available => "Available",
            DealerStatus::Flying => "Flying",
            DealerStatus::Dead(_) => "Dead",
        }
    }

    pub fn add_local<T: DealerComponent>(&mut self, name: &str, amount: usize, bought_at: u128) {
        let location = self.location.clone();
        self.add_at::<T>(&location, name, amount, bought_at)
    }

    pub fn add_at<T: DealerComponent>(
        &mut self,
        location: &str,
        name: &str,
        amount: usize,
        bought_at: u128,
    ) {
        if amount == 0 {
            return;
        }

        let owned_elements = T::get_elements_at_mut(self, location);

        let owned_element = match owned_elements.entry(name.to_owned()) {
            std::collections::hash_map::Entry::Occupied(o) => o.into_mut(),
            std::collections::hash_map::Entry::Vacant(v) => v.insert(OwnedElement::default()),
        };

        let average_price = (owned_element.amount as u128 * owned_element.bought_at
            + amount as u128 * bought_at)
            / (owned_element.amount as u128 + amount as u128);

        owned_element.amount += amount;
        owned_element.bought_at = average_price;
    }

    pub fn sub_local<T: DealerComponent>(&mut self, name: &str, amount: usize) {
        let location = self.location.clone();
        self.sub_at::<T>(&location, name, amount);
    }

    pub fn sub_at<T: DealerComponent>(&mut self, location: &str, name: &str, amount: usize) {
        if amount == 0 {
            return;
        }

        let owned_elements = T::get_elements_at_mut(self, location);

        let owned_element = match owned_elements.entry(name.to_owned()) {
            std::collections::hash_map::Entry::Occupied(o) => o.into_mut(),
            std::collections::hash_map::Entry::Vacant(v) => v.insert(OwnedElement::default()),
        };

        owned_element.amount -= amount;

        if owned_element.amount <= 0 {
            owned_elements.remove(name);
        }
    }

    pub fn can_use_weapon(&self, weapon_name: &str, maybe_ammo: Option<&str>) -> Result<()> {
        let local_owned = self.owned_items.get(&self.location).unwrap();

        let Some(_) = local_owned.get(weapon_name) else {
            return Err(Error::WeaponNotFound(weapon_name.to_owned()));
        };

        if maybe_ammo.is_some() {
            let Some(ammo) = local_owned.get(&maybe_ammo.unwrap().to_owned()) else {
                return Err(Error::NoElementOwned(maybe_ammo.unwrap().to_owned()));
            };

            if ammo.amount < 1 {
                return Err(Error::NotEnoughElementOwned(maybe_ammo.unwrap().to_owned()));
            }
        }

        Ok(())
    }

    pub fn get_best_armor(&self, drug_wars: &DrugWars) -> Option<(String, Armor)> {
        let local_owned = self.owned_items.get(&self.location).unwrap();

        local_owned
            .iter()
            .filter_map(|(item_str, _)| {
                let item_str = item_str.replace(" ", "").to_lowercase();

                let Ok((item_name, item)) = drug_wars.get_matching::<Item>(&item_str) else {
                    return None
                };

                match &item.kind {
                    ItemKind::Armor(armor) => Some((item_name.to_owned(), armor.clone())),
                    _ => None,
                }
            })
            .sorted_by_key(|(_, armor)| (armor.block * 1000.) as u32)
            .rev()
            .last()
    }

    pub fn get_random<T: DealerComponent>(
        &self,
        rng: &mut dyn RngCore,
    ) -> Option<(String, OwnedElement)> {
        let owned_elements = self.get_owned_local::<T>();

        let element = owned_elements.iter().choose(rng);
        if element.is_none() {
            return None;
        }

        Some((element.unwrap().0.clone(), element.unwrap().1.clone()))
    }
}
