use std::collections::{HashMap, HashSet};

use crate::{
    drug_wars::DrugWars,
    utils::{DealerComponent, Matchable},
};
use chrono::NaiveDate;
use itertools::Itertools;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum MessageKind {
    RumorUpHere,
    RumorDownHere,
    RumorUpAt,
    RumorDownAt,
    Welcome,
    PriceUp,
    PriceUpEnd,
    PriceDown,
    PriceDownEnd,
}

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct Drug {
    pub nominal_price: u128,
}

impl Matchable for Drug {
    fn get_matching_elements<'a>(
        drug_wars: &'a DrugWars,
        name: &'a str,
    ) -> Vec<(&'a String, &'a Self)>
    where
        Self: Sized,
    {
        let name = name.to_lowercase();
        drug_wars
            .drugs
            .iter()
            .filter(|(elem_name, _)| elem_name.to_lowercase().replace(" ", "").starts_with(&name))
            .collect::<Vec<_>>()
    }
}

impl DealerComponent for Drug {
    fn get_elements_at<'a>(
        dealer: &'a crate::dealer::Dealer,
        location: &'a str,
    ) -> &'a HashMap<String, OwnedElement> {
        dealer.owned_drugs.get(location).unwrap()
    }

    fn get_elements_at_mut<'a>(
        dealer: &'a mut crate::dealer::Dealer,
        location: &'a str,
    ) -> &'a mut HashMap<String, OwnedElement> {
        dealer.owned_drugs.get_mut(location).unwrap()
    }

    fn get_market_at<'a>(location: &'a Location) -> &'a HashMap<String, MarketElement> {
        &location.drug_market
    }

    fn get_market_at_mut<'a>(location: &'a mut Location) -> &'a mut HashMap<String, MarketElement> {
        &mut location.drug_market
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Weapon {
    pub ammo: Option<String>,
    pub damage: f32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Armor {
    pub block: f32,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoScent {
    pub capacity: usize,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ItemKind {
    Weapon(Weapon),
    Ammo,
    Armor(Armor),
    NoScent(NoScent),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Item {
    pub nominal_price: u128,
    pub kind: ItemKind,
}

impl DealerComponent for Item {
    fn get_elements_at<'a>(
        dealer: &'a crate::dealer::Dealer,
        location: &'a str,
    ) -> &'a HashMap<String, OwnedElement> {
        dealer.owned_items.get(location).unwrap()
    }

    fn get_elements_at_mut<'a>(
        dealer: &'a mut crate::dealer::Dealer,
        location: &'a str,
    ) -> &'a mut HashMap<String, OwnedElement> {
        dealer.owned_items.get_mut(location).unwrap()
    }

    fn get_market_at<'a>(location: &'a Location) -> &'a HashMap<String, MarketElement> {
        &location.item_market
    }

    fn get_market_at_mut<'a>(location: &'a mut Location) -> &'a mut HashMap<String, MarketElement> {
        &mut location.item_market
    }
}

impl Matchable for Item {
    fn get_matching_elements<'a>(
        drug_wars: &'a DrugWars,
        name: &'a str,
    ) -> Vec<(&'a String, &'a Self)>
    where
        Self: Sized,
    {
        let name = name.to_lowercase();
        drug_wars
            .items
            .iter()
            .filter(|(elem_name, _)| elem_name.to_lowercase().replace(" ", "").starts_with(&name))
            .sorted_by_key(|k| k.0.len())
            .collect::<Vec<_>>()
    }
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct OwnedElement {
    pub amount: usize,
    pub bought_at: u128,
}

const CHUNK: usize = 200;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MarketElement {
    pub supply: usize,
    pub demand: usize,
    pub price: u128,
}

impl MarketElement {
    pub fn buy(&mut self, amount: usize) -> u128 {
        let loops = amount / CHUNK;
        let rest = amount % CHUNK;

        let mut price = 0;
        for _ in 0..loops {
            price += self.price * CHUNK as u128;
            self.extract(CHUNK);
        }
        price += self.price * rest as u128;
        self.extract(rest);

        price
    }

    pub fn sell(&mut self, amount: usize) -> u128 {
        let loops = amount / CHUNK;
        let rest = amount % CHUNK;

        let mut price = 0;
        for _ in 0..loops {
            price += self.price * CHUNK as u128;
            self.inject(CHUNK);
        }
        price += self.price * rest as u128;
        self.inject(rest);

        price
    }

    pub fn inject(&mut self, amount: usize) {
        let deviation = self.demand as f64 - self.supply as f64;
        let current_ratio = 1. + (deviation / ((self.supply as f64).max(self.demand as f64) * 1.7));

        let f_price = (self.price as f64) / 10000.;
        let unit_price = f_price / (current_ratio * 100.);

        self.supply += amount;
        self.demand -= amount;

        let deviation = self.demand as f64 - self.supply as f64;
        let new_ratio = 1. + (deviation / ((self.supply as f64).max(self.demand as f64) * 1.7));

        let new_f_price = unit_price * new_ratio * 100.;
        self.price = (new_f_price * 10000.) as u128;
    }

    pub fn extract(&mut self, amount: usize) {
        let deviation = self.demand as f64 - self.supply as f64;
        let current_ratio = 1. + (deviation / ((self.supply as f64).max(self.demand as f64) * 1.7));

        let f_price = (self.price as f64) / 10000.;
        let unit_price = f_price / (current_ratio * 100.);

        self.supply -= amount;
        self.demand += amount;

        let deviation = self.demand as f64 - self.supply as f64;
        let new_ratio = 1. + (deviation / ((self.supply as f64).max(self.demand as f64) * 1.7));

        let new_f_price = unit_price * new_ratio * 100.;
        self.price = (new_f_price * 10000.) as u128;
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Location {
    pub lat: f32,
    pub long: f32,
    pub drug_market: HashMap<String, MarketElement>,
    pub item_market: HashMap<String, MarketElement>,
    pub messages: Vec<String>,
    pub blokes: HashSet<String>,
    pub price_mods: Vec<PriceMod>,
    pub rumors: Vec<Rumor>,
}

impl Matchable for Location {
    fn get_matching_elements<'a>(
        drug_wars: &'a DrugWars,
        name: &'a str,
    ) -> Vec<(&'a String, &'a Self)>
    where
        Self: Sized,
    {
        let name = name.to_lowercase();
        drug_wars
            .locations
            .iter()
            .filter(|(elem_name, _)| elem_name.to_lowercase().contains(&name))
            .collect::<Vec<_>>()
    }
}

impl Location {
    pub fn get_market<T: DealerComponent>(&self) -> &HashMap<String, MarketElement> {
        T::get_market_at(self)
    }

    pub fn get_market_mut<T: DealerComponent>(&mut self) -> &mut HashMap<String, MarketElement> {
        T::get_market_at_mut(self)
    }
}

pub struct Settings {
    pub day_duration: u32,
    pub current_day: NaiveDate,
    pub save_path: String,
    pub config_path: String,
    pub width: usize,
}

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct Shipment {
    pub owner: String,
    pub element: String,
    pub amount: usize,
    pub destination: String,
    pub bought_at: u128,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum DealerStatus {
    Available,
    Flying,
    Dead(NaiveDate),
}

impl std::fmt::Display for DealerStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DealerStatus::Available => write!(f, ""),
            DealerStatus::Flying => write!(f, "can't do business while flying"),
            DealerStatus::Dead(_) => write!(f, "can't do business while dead"),
        }
    }
}

#[derive(PartialEq, Eq)]
pub enum AmountAction {
    Buying,
    Selling,
    Shipping,
    Capacity,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum PriceTrend {
    Up,
    Down,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum PriceModKind {
    Rumor,
    Spontaneous,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PriceMod {
    pub drug: String,
    pub trend: PriceTrend,
    pub kind: PriceModKind,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Rumor {
    pub drug: String,
    pub trend: PriceTrend,
    pub location: String,
    pub confirmed: Option<bool>,
}
