use crate::definitions::DealerStatus;

pub type Error = DrugWarsError;
pub type Result<T> = std::result::Result<T, DrugWarsError>;

#[derive(Debug, Clone)]
pub enum DrugWarsError {
    DealerNotPlaying,
    DealerNotAvailable(DealerStatus),
    CantAttackDealer(String, DealerStatus),
    DealerAlreadyRegistered,
    InvalidSize,
    NoElementAtMarket(String),
    NoElementOwned(String),
    NotEnoughSupply(String),
    NotEnoughDemand(String),
    NotEnoughElementOwned(String),
    NotEnoughCapacity,
    NotEnoughCapacityAt(String),
    CapacityOverflow,
    NotEnoughMoney,
    NegativeMoney,
    LaunderNegativeMoney,
    BlokeNotHere(String),
    BlokeNotEnoughCapacity(String),
    ShipCurrentLocation,
    ElementNotFound(String),
    ElementAmbiguous(String),
    ParseError(String),
    HashMapKeyNotFound(String),
    WeaponNotFound(String),
    NotSameLocation(String),
    ItemNotWeapon(String),
    DealerNotFound(String),
    AlreadyAttacked,
    AlreadyFullHealth,
    DealerNotDead(String),
    AlreadyLooted(String),
    AmountIsZero,
    UnknownStatus(String),
    UnknownDate(String),
}

impl std::fmt::Display for DrugWarsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            DrugWarsError::DealerNotPlaying => write!(f, "you aren't playing yet you donkey"),
            DrugWarsError::DealerNotAvailable(status) => {
                write!(f, "{}", status)
            }
            DrugWarsError::CantAttackDealer(nick, status) => {
                write!(f, "can't attack {} -> {}", nick, status)
            }
            DrugWarsError::DealerAlreadyRegistered => {
                write!(f, "you're already registered you bloot clot donkey")
            }
            DrugWarsError::InvalidSize => write!(f, "size must be between 75 and 130"),
            DrugWarsError::NoElementAtMarket(name) => {
                write!(f, "there isn't any {} on the market today.", name)
            }
            DrugWarsError::NoElementOwned(name) => {
                write!(f, "you don't have any {} here.", name)
            }
            DrugWarsError::NotEnoughSupply(name) => {
                write!(f, "there isn't enough supply of {} today.", name)
            }
            DrugWarsError::NotEnoughDemand(name) => {
                write!(f, "there isn't enough demand of {} today.", name)
            }
            DrugWarsError::NotEnoughElementOwned(name) => {
                write!(f, "you don't own enough {}", name)
            }
            DrugWarsError::NotEnoughCapacity => write!(f, "you don't have enough capacity"),
            DrugWarsError::NotEnoughCapacityAt(name) => {
                write!(f, "you don't have enough capacity at {}", name)
            }
            DrugWarsError::CapacityOverflow => {
                write!(f, "you won't ever need that much capacity. will you?")
            }
            DrugWarsError::NotEnoughMoney => {
                write!(f, "you don't have enough money you broke ass punk")
            }
            DrugWarsError::NegativeMoney => write!(f, "you can't give negative money"),
            DrugWarsError::LaunderNegativeMoney => write!(f, "you can't launder negative money"),
            DrugWarsError::BlokeNotHere(block_name) => {
                write!(f, "{} isn't at your current location", block_name)
            }
            DrugWarsError::BlokeNotEnoughCapacity(block_name) => {
                write!(f, "{} don't have enough capacity", block_name)
            }
            DrugWarsError::ShipCurrentLocation => {
                write!(f, "you can't ship to your current location")
            }
            DrugWarsError::ElementNotFound(name) => write!(f, "couldn't find {}", name),
            DrugWarsError::ElementAmbiguous(name) => write!(f, "{} is too ambiguous", name),
            DrugWarsError::ParseError(val) => write!(f, "unable to parse '{}'", val),
            DrugWarsError::HashMapKeyNotFound(key) => write!(f, "hashmap key {} not found", key),
            DrugWarsError::WeaponNotFound(weapon) => {
                write!(f, "you don't own any {} here.", weapon)
            }
            DrugWarsError::NotSameLocation(target_nick) => {
                write!(f, "you are not at the same place as {}", target_nick)
            }
            DrugWarsError::ItemNotWeapon(weapon) => {
                write!(f, "{} is not a weapon", weapon)
            }
            DrugWarsError::DealerNotFound(nick) => {
                write!(f, "{} is not a player nick", nick)
            }
            DrugWarsError::AlreadyAttacked => {
                write!(f, "you already attacked today")
            }
            DrugWarsError::AlreadyFullHealth => {
                write!(f, "you already have 100 hp you donut")
            }
            DrugWarsError::DealerNotDead(nick) => {
                write!(f, "{} isn't dead yet", nick)
            }
            DrugWarsError::AlreadyLooted(nick) => {
                write!(f, "you already looted {}", nick)
            }
            DrugWarsError::AmountIsZero => {
                write!(f, "can't have an amount equals zero")
            }
            DrugWarsError::UnknownStatus(status_str) => {
                write!(f, "can't find status {}", status_str)
            }
            DrugWarsError::UnknownDate(date_str) => {
                write!(f, "can't parse date {}", date_str)
            }
        }
    }
}

impl std::error::Error for DrugWarsError {}
