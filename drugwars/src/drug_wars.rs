use std::{
    collections::{HashMap, HashSet},
    fmt::Debug,
    str::FromStr,
    time::SystemTime,
};

use chrono::{Duration, NaiveDate};
use irc::{format::IrcColor, privmsg::PrivMsg, Irc};
use rand::{rngs::StdRng, seq::SliceRandom, Rng, SeedableRng};

use crate::{
    config::DrugWarsConfig,
    dealer::Dealer,
    definitions::{
        Armor, DealerStatus, Drug, Item, ItemKind, Location, MarketElement, MessageKind, PriceMod,
        PriceModKind, PriceTrend, Rumor, Settings, Shipment, Weapon,
    },
    error::{Error, Result},
    save::DrugWarsSave,
    utils::{
        calc_damage, capacity_price, get_flight_price, get_shipping_price, hl_error, hl_message,
        pretty_print_amount, pretty_print_money, DealerComponent, Matchable,
    },
};

pub struct DrugWars {
    pub settings: Settings,
    pub rng: StdRng,
    pub timer: SystemTime,
    pub dealers: HashMap<String, Dealer>,
    pub locations: HashMap<String, Location>,
    pub flights: HashMap<String, String>,
    pub shipments: Vec<Shipment>,
    pub drugs: HashMap<String, Drug>,
    pub items: HashMap<String, Item>,
    pub messages: HashMap<MessageKind, Vec<String>>,
    pub laundering_fees: f32,
}

impl From<DrugWarsConfig> for DrugWars {
    fn from(config: DrugWarsConfig) -> Self {
        let mut locations = HashMap::default();
        let mut drugs = HashMap::default();
        let mut items = HashMap::default();
        let mut messages = HashMap::default();

        for drug in config.drugs {
            let name = drug.as_mapping().unwrap()["name"].as_str().unwrap();
            let price = drug.as_mapping().unwrap()["price"].as_f64().unwrap();
            drugs.insert(
                name.to_owned(),
                Drug {
                    nominal_price: (price * 10000.) as u128,
                },
            );
        }

        let weapons = config.items["weapons"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for weapon in weapons {
            let name = weapon["name"].as_str().unwrap();
            let price = weapon["price"].as_f64().unwrap();
            let damage = weapon["damage"].as_f64().unwrap() as f32;

            let mut ammo = None;

            if weapon.contains_key("ammo") {
                ammo = Some(weapon["ammo"].as_str().unwrap().to_owned())
            }

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Weapon(Weapon { ammo, damage }),
                },
            );
        }

        let ammos = config.items["ammos"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for ammo in ammos {
            let name = ammo["name"].as_str().unwrap();
            let price = ammo["price"].as_f64().unwrap();

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Ammo,
                },
            );
        }

        let armors = config.items["armors"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for armor in armors {
            let name = armor["name"].as_str().unwrap();
            let price = armor["price"].as_f64().unwrap();
            let block = armor["block"].as_f64().unwrap() as f32;

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Armor(Armor { block }),
                },
            );
        }

        for location in config.locations {
            let name = location.as_mapping().unwrap()["name"].as_str().unwrap();
            let lat = location.as_mapping().unwrap()["position"]
                .as_mapping()
                .unwrap()["lat"]
                .as_f64()
                .unwrap() as f32;
            let long = location.as_mapping().unwrap()["position"]
                .as_mapping()
                .unwrap()["long"]
                .as_f64()
                .unwrap() as f32;

            locations.insert(
                name.to_owned(),
                Location {
                    lat,
                    long,
                    drug_market: HashMap::default(),
                    item_market: HashMap::default(),
                    messages: vec![],
                    blokes: HashSet::default(),
                    price_mods: vec![],
                    rumors: vec![],
                },
            );
        }

        // OH LOOK ! I'M FUCKING SLEEP DEPRIVATED !
        for (val_str, enum_variant) in [
            ("price_up", MessageKind::PriceUp),
            ("price_up_end", MessageKind::PriceUpEnd),
            ("price_down", MessageKind::PriceDown),
            ("price_down_end", MessageKind::PriceDownEnd),
        ] {
            let msgs = &config.messages[val_str].as_sequence().unwrap();
            for msg in *msgs {
                let message_vec = messages.entry(enum_variant).or_insert_with(|| vec![]);
                message_vec.push(msg.as_str().unwrap().to_owned());
            }
        }

        let day_duration = config.settings["day_duration"].as_u64().unwrap() as u32;
        let current_day_str = config.settings["start_day"].as_str().unwrap();
        let save_path = config.settings["save_path"].as_str().unwrap();
        let width = config.settings["width"].as_u64().unwrap();

        let mut rng = StdRng::from_entropy();

        let laundering_fees = rng.gen_range((0.05)..=(0.2));

        Self {
            settings: Settings {
                day_duration,
                current_day: NaiveDate::from_str(current_day_str).unwrap(),
                save_path: save_path.to_owned(),
                config_path: config.config_path.unwrap(),
                width: width as usize,
            },
            rng,
            timer: SystemTime::now(),
            dealers: HashMap::default(),
            locations,
            shipments: vec![],
            flights: HashMap::default(),
            drugs,
            items,
            messages,
            laundering_fees,
        }
    }
}

impl DrugWars {
    pub fn load_config(path: &str) -> Self {
        let config = DrugWarsConfig::from_file(path).unwrap();
        let Some(save_path) = config.settings["save_path"].as_str() else {
            return config.into();
        };

        match DrugWarsSave::load(save_path) {
            Ok(save) => save.into(),
            Err(_) => config.into(),
        }
    }

    pub fn init(&mut self) {
        self.update_price_mods();
        self.confirm_rumors();

        self.update_markets();
        self.generate_rumors();
    }

    fn update_price_mods(&mut self) {
        for (_, location) in &mut self.locations {
            location.price_mods.clear();

            for (drug_name, _) in &self.drugs {
                if self.rng.gen_bool(0.92) {
                    continue;
                }

                match self.rng.gen_bool(1. / 2.) {
                    // Price down
                    true => location.price_mods.push(PriceMod {
                        drug: drug_name.clone(),
                        trend: PriceTrend::Down,
                        kind: PriceModKind::Spontaneous,
                    }),
                    // Price UP !
                    false => location.price_mods.push(PriceMod {
                        drug: drug_name.clone(),
                        trend: PriceTrend::Up,
                        kind: PriceModKind::Spontaneous,
                    }),
                }
            }
        }
    }

    fn confirm_rumors(&mut self) {
        for (_, location) in &mut self.locations {
            location.rumors.retain(|rumor| rumor.confirmed.is_none());

            for rumor in &mut location.rumors {
                rumor.confirmed = Some(self.rng.gen_bool(1. / 2.));
            }
        }

        let all_rumors = self
            .locations
            .iter()
            .flat_map(|(_, l)| l.rumors.clone())
            .collect::<Vec<_>>();

        for rumor in all_rumors {
            if let Some(true) = rumor.confirmed {
                let location = self.locations.get_mut(&rumor.location).unwrap();
                location.price_mods.push(PriceMod {
                    drug: rumor.drug.clone(),
                    trend: rumor.trend.clone(),
                    kind: PriceModKind::Rumor,
                });
            }
        }
    }

    fn generate_rumors(&mut self) {
        let locations = self.locations.keys().cloned().collect::<Vec<_>>();

        for (_, location) in &mut self.locations {
            location.rumors.clear();

            for (drug_name, _) in &self.drugs {
                if self.rng.gen_bool(0.95) {
                    continue;
                }

                match self.rng.gen_bool(1. / 2.) {
                    // Price down
                    true => location.rumors.push(Rumor {
                        drug: drug_name.clone(),
                        trend: PriceTrend::Down,
                        location: locations.choose(&mut self.rng).unwrap().clone(),
                        confirmed: None,
                    }),
                    // Price UP !
                    false => location.rumors.push(Rumor {
                        drug: drug_name.clone(),
                        trend: PriceTrend::Up,
                        location: locations.choose(&mut self.rng).unwrap().clone(),
                        confirmed: None,
                    }),
                }
            }
        }
    }

    fn update_markets(&mut self) {
        for (_, location) in &mut self.locations {
            location.drug_market.clear();
            location.item_market.clear();

            for (drug_name, drug) in &self.drugs {
                let mods = location
                    .price_mods
                    .clone()
                    .into_iter()
                    .filter(|price_mod| *price_mod.drug == *drug_name)
                    .collect::<Vec<_>>();

                if self.rng.gen_bool(4. / 5.) && mods.len() == 0 {
                    continue;
                };

                let supply = self.rng.gen_range(0..1000000);
                let demand = self.rng.gen_range(0..1000000);

                let deviation = demand as f64 - supply as f64;

                let ratio = 1. + (deviation / ((supply as f64).max(demand as f64) * 1.7));

                let float_price = ((drug.nominal_price as f64) / 10000.) * ratio;
                let mut price = (float_price * 10000.) as u128;

                for price_mod in mods {
                    match price_mod.trend {
                        PriceTrend::Up => price *= 15,
                        PriceTrend::Down => price /= 6,
                    }
                }

                location.drug_market.insert(
                    drug_name.clone(),
                    MarketElement {
                        supply,
                        demand,
                        price,
                    },
                );
            }

            for (item_name, item) in &self.items {
                if self.rng.gen_bool(4. / 5.) {
                    continue;
                };

                let supply = self.rng.gen_range(0..1000000);
                let demand = self.rng.gen_range(0..1000000);

                let deviation = demand as f64 - supply as f64;

                let ratio = 1. + (deviation / ((supply as f64).max(demand as f64) * 1.7));

                let float_price = ((item.nominal_price as f64) / 10000.) * ratio;
                let price = (float_price * 10000.) as u128;

                location.item_market.insert(
                    item_name.clone(),
                    MarketElement {
                        supply,
                        demand,
                        price,
                    },
                );
            }
        }
    }

    pub fn check_new_day(&mut self, irc: &mut Irc) {
        let Ok(elapsed) = self.timer.elapsed() else { return; };

        if elapsed.as_secs_f32() > self.settings.day_duration as f32 {
            let lines = self.new_day().unwrap();

            for line in lines {
                irc.privmsg_all(&line);
            }
        }
    }

    pub fn new_day(&mut self) -> Result<Vec<String>> {
        let mut msg = PrivMsg::new();
        let msg = msg
            .text("new day: ")
            .color(IrcColor::LightGreen)
            .text(&self.get_date())
            .get();

        let mut lines = vec![msg.to_owned()];

        self.laundering_fees = self.rng.gen_range((0.05)..=(0.2));

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("today's laundering fees: ")
            .color(IrcColor::Cyan)
            .text(&format!("{:.2}%", self.laundering_fees * 100.))
            .get();

        lines.push(msg.to_owned());

        self.timer = SystemTime::now();
        self.settings.current_day += Duration::days(1);

        // do price mods
        self.update_price_mods();
        // do rumor truthness
        self.confirm_rumors();

        for (nick, destination) in &mut self.flights.clone() {
            let dealer = self.get_dealer_mut(nick).unwrap();

            let mut msg = PrivMsg::new();
            let msg = msg
                .color(IrcColor::Green)
                .text("you landed at ")
                .color(IrcColor::Purple)
                .text(&destination)
                .get();

            lines.append(&mut hl_message(&nick, msg));
            dealer.location = destination.clone();
            dealer.status = DealerStatus::Available;

            let location = self.locations.get_mut(destination.as_str()).unwrap();
            location.blokes.insert(nick.clone());
        }

        for shipment in &mut self.shipments.clone() {
            let mut msg = PrivMsg::new();
            let msg = msg
                .color(IrcColor::Green)
                .text("you shipment of ")
                .color(IrcColor::Yellow)
                .text(&format!(
                    "{} {}",
                    pretty_print_amount(shipment.amount),
                    shipment.element
                ))
                .reset()
                .text(" arrived at ")
                .color(IrcColor::Purple)
                .text(&shipment.destination)
                .get();

            lines.append(&mut hl_message(&shipment.owner, msg));

            let is_drug = self.get_matching::<Drug>(&shipment.element).is_ok();

            let dealer = self.get_dealer_mut(&shipment.owner).unwrap();

            // todo: use generics insteam
            if is_drug {
                dealer.add_at::<Drug>(
                    &shipment.destination,
                    &shipment.element,
                    shipment.amount,
                    shipment.bought_at,
                );
            } else {
                dealer.add_at::<Item>(
                    &shipment.destination,
                    &shipment.element,
                    shipment.amount,
                    shipment.bought_at,
                );
            }
        }

        let mut respawn_msg = PrivMsg::new();
        let respawn_msg = respawn_msg
            .color(IrcColor::Green)
            .text("You respawned!")
            .get();

        for (dealer_nick, dealer) in &mut self.dealers {
            dealer.has_attacked = false;
            if let DealerStatus::Dead(from) = dealer.status {
                if self.settings.current_day - from == Duration::days(7) {
                    lines.append(&mut hl_message(dealer_nick, respawn_msg));
                    dealer.health = 100.;
                    dealer.status = DealerStatus::Available;
                    dealer.looters = HashSet::default();
                }
            }
        }

        self.flights.clear();
        self.shipments.clear();

        self.update_markets();

        // make new rumors
        self.generate_rumors();

        let save = DrugWarsSave::from(self);
        if let Err(e) = save.save() {
            println!("{:?}", e);
        };

        Ok(lines)
    }

    pub fn get_dealer(&self, nick: &str) -> Result<&Dealer> {
        match self.dealers.get(nick) {
            Some(dealer) => Ok(dealer),
            None => Err(Error::DealerNotPlaying),
        }
    }

    pub fn get_dealer_available(&self, nick: &str) -> Result<&Dealer> {
        let dealer = match self.dealers.get(nick) {
            Some(dealer) => dealer,
            None => return Err(Error::DealerNotPlaying),
        };

        if !dealer.available() {
            return Err(Error::DealerNotAvailable(dealer.status.clone()));
        }

        Ok(dealer)
    }

    pub fn get_dealer_mut(&mut self, nick: &str) -> Result<&mut Dealer> {
        match self.dealers.get_mut(nick) {
            Some(dealer) => Ok(dealer),
            None => Err(Error::DealerNotPlaying),
        }
    }

    pub fn get_matching<'a, T: Matchable>(&'a self, name: &'a str) -> Result<(&'a String, &'a T)> {
        let matching_elements = T::get_matching_elements(self, &name);

        if matching_elements.len() == 0 {
            return Err(Error::ElementNotFound(name.to_owned()));
        }

        Ok(matching_elements[0])
    }

    pub fn get_amount_from_str<F: FromStr>(&self, amount_str: &str) -> Result<F>
    where
        <F as FromStr>::Err: Debug,
    {
        //TODO: let users use `max` as amount

        let Ok(amount) = amount_str.parse::<F>() else {
            return Err(Error::ParseError(amount_str.to_owned()));
        };

        if amount_str.parse::<usize>().unwrap() <= 0 {
            return Err(Error::AmountIsZero);
        }

        return Ok(amount);
    }

    pub fn get_sell_amount_of<T: DealerComponent>(
        &self,
        dealer: &Dealer,
        elem_name: &str,
        amount_str: &str,
    ) -> Result<usize> {
        if amount_str.to_lowercase() != "max" {
            let Ok(amount) = amount_str.parse::<usize>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };

            return Ok(amount);
        }

        let owned_elements = dealer.get_owned_local::<T>();

        let market = self
            .locations
            .get(&dealer.location)
            .unwrap()
            .get_market::<T>();

        let Some(market_elem) = market.get(elem_name) else {
            return Err(Error::ElementNotFound(elem_name.to_owned()));
        };

        let Some(owned_elem) = owned_elements.get(elem_name) else {
            return Err(Error::ElementNotFound(elem_name.to_owned()));
        };

        Ok(market_elem.demand.min(owned_elem.amount))
    }

    pub fn get_buy_amount_of<T: DealerComponent>(
        &self,
        dealer: &Dealer,
        elem_name: &str,
        amount_str: &str,
    ) -> Result<usize> {
        if amount_str.to_lowercase() != "max" {
            let Ok(amount) = amount_str.parse::<usize>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };

            return Ok(amount);
        }

        let remaining_capacity = dealer.get_remaining_capacity_local::<T>();

        let location = self.locations.get(&dealer.location).unwrap();

        let market = location.get_market::<T>();

        let Some(market_elem) = market.get(elem_name) else {
            return Err(Error::ElementNotFound(elem_name.to_owned()));
        };

        let max_money_can_buy = dealer.money / market_elem.price;

        Ok(remaining_capacity
            .min(market_elem.supply)
            .min(max_money_can_buy as usize))
    }

    pub fn get_give_amount_of<T: DealerComponent>(
        &self,
        dealer: &Dealer,
        target_dealer: &Dealer,
        elem_name: &str,
        amount_str: &str,
    ) -> Result<usize> {
        if amount_str.to_lowercase() != "max" {
            let Ok(amount) = amount_str.parse::<usize>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };

            return Ok(amount);
        }

        let target_remaining_cap = target_dealer.get_remaining_capacity_local::<T>();

        let owned_elements = dealer.get_owned_local::<T>();
        let Some(owned_elem) = owned_elements.get(elem_name) else {
            return Err(Error::ElementNotFound(elem_name.to_owned()));
        };

        Ok(owned_elem.amount.min(target_remaining_cap))
    }

    pub fn get_ship_amount_of<T: DealerComponent>(
        &self,
        dealer: &Dealer,
        destination_name: &str,
        elem_name: &str,
        amount_str: &str,
    ) -> Result<usize> {
        if amount_str.to_lowercase() != "max" {
            let Ok(amount) = amount_str.parse::<usize>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };

            return Ok(amount);
        }

        let dest_cap = dealer.get_remaining_capacity_at::<T>(destination_name);

        let owned_elements = dealer.get_owned_local::<T>();
        let Some(owned_elem) = owned_elements.get(elem_name) else {
            return Err(Error::ElementNotFound(elem_name.to_owned()));
        };

        Ok(owned_elem.amount.min(dest_cap))
    }

    pub fn get_money_amount(&self, dealer: &Dealer, amount_str: &str) -> Result<u128> {
        if amount_str.to_lowercase() != "max" {
            let Ok(amount) = amount_str.parse::<f64>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };
            let amount = (amount * 10000.) as u128;

            if dealer.money < amount {
                return Err(Error::NotEnoughMoney);
            }
            return Ok(amount);
        }

        Ok(dealer.money)
    }

    pub fn get_laundering_amount(&self, dealer: &Dealer, amount_str: &str) -> Result<(u128, u128)> {
        if amount_str.to_lowercase() != "max" {
            let Ok(original) = amount_str.parse::<f64>() else {
                return Err(Error::ParseError(amount_str.to_owned()));
            };

            if original <= 0. {
                return Err(Error::LaunderNegativeMoney);
            }

            let fees = original * self.laundering_fees as f64;

            let f_amount = original - fees;
            let amount = (f_amount * 10000.) as u128;

            if dealer.money < (original * 10000.) as u128 {
                return Err(Error::NotEnoughMoney);
            }

            return Ok((amount, (original * 10000.) as u128));
        }

        let final_amount = (dealer.money / 10000) as f64;
        let unit = final_amount / ((1. + self.laundering_fees as f64) * 100.);
        let original = unit * 100.;
        let fees = final_amount - original;
        let laundered = final_amount - fees;

        Ok(((laundered * 10000.) as u128, dealer.money))
    }

    pub fn _buy<T: DealerComponent>(
        &mut self,
        nick: &str,
        name: &str,
        amount: usize,
        bought_at: u128,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;

        let location = self.locations.get_mut(&dealer.location.clone()).unwrap();
        let market = location.get_market_mut::<T>();
        let market_element = market.get_mut(name).unwrap();
        let price = market_element.buy(amount);

        let dealer = self.get_dealer_mut(nick).unwrap();
        dealer.add_local::<T>(name, amount, bought_at);
        dealer.money -= price;

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you bought ")
            .color(IrcColor::Yellow)
            .text(&format!("{} {}", pretty_print_amount(amount), name))
            .reset()
            .text(" for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _sell<T: DealerComponent>(
        &mut self,
        nick: &str,
        name: &str,
        amount: usize,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;

        let location = self.locations.get_mut(&dealer.location.clone()).unwrap();
        let market = location.get_market_mut::<T>();
        let market_element = market.get_mut(name).unwrap();
        let price = market_element.sell(amount);

        let dealer = self.get_dealer_mut(nick).unwrap();
        dealer.sub_local::<T>(name, amount);
        dealer.money += price;

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you sold ")
            .color(IrcColor::Yellow)
            .text(&format!("{} {}", pretty_print_amount(amount), name))
            .reset()
            .text(" for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _ship<T: DealerComponent>(
        &mut self,
        nick: &str,
        name: &str,
        amount: usize,
        destination_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;

        let elem = dealer.get_owned_local::<T>().get(name).unwrap();

        let shipment = Shipment {
            owner: nick.to_owned(),
            element: name.to_owned(),
            amount,
            destination: destination_str.to_owned(),
            bought_at: elem.bought_at,
            ..Default::default()
        };

        let dealer_location = self.locations.get(&dealer.location).unwrap();
        let destination = self.locations.get(destination_str).unwrap();
        let price = get_shipping_price(dealer_location, destination, amount);

        let dealer = self.get_dealer_mut(nick)?;
        dealer.sub_local::<T>(name, amount);
        dealer.money -= price;

        self.shipments.push(shipment);

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you shipped ")
            .color(IrcColor::Yellow)
            .text(&format!("{} {}", pretty_print_amount(amount), name))
            .reset()
            .text(" to ")
            .color(IrcColor::Purple)
            .text(destination_str)
            .reset()
            .text(" for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .reset()
            .text(". your shipment will arrive tomorrow.")
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _give_money(
        &mut self,
        nick: &str,
        bloke_nick: &str,
        money: u128,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_mut(nick).unwrap();
        dealer.money -= money;

        let bloke = self.get_dealer_mut(bloke_nick).unwrap();
        bloke.money += money;

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you gave ")
            .color(IrcColor::Yellow)
            .text(bloke_nick)
            .text(" ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(money))
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _give<T: DealerComponent>(
        &mut self,
        nick: &str,
        bloke_nick: &str,
        name: &str,
        amount: usize,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_mut(nick).unwrap();
        dealer.sub_local::<T>(name, amount);

        let bloke = self.get_dealer_mut(bloke_nick).unwrap();
        bloke.add_local::<T>(name, amount, 0);

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you gave ")
            .color(IrcColor::Yellow)
            .text(&format!("{} {}", pretty_print_amount(amount), name))
            .reset()
            .text(" to ")
            .color(IrcColor::Purple)
            .text(bloke_nick)
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _fly_to(&mut self, nick: &str, destination_name: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick).unwrap();
        let current_location = self.locations.get(&dealer.location).unwrap();

        let destination = self.locations.get(destination_name).unwrap();

        let price = get_flight_price(current_location, destination);

        let current_location = self.locations.get_mut(&dealer.location.clone()).unwrap();
        current_location.blokes.remove(nick);

        let dealer = self.get_dealer_mut(nick).unwrap();

        dealer.money -= price;
        dealer.status = DealerStatus::Flying;

        self.flights
            .insert(nick.to_string(), destination_name.to_owned());

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you take a flight to ")
            .color(IrcColor::Purple)
            .text(destination_name)
            .reset()
            .text(" for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .reset()
            .text(". You'll arrive tomorrow")
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn _buy_capacity(&mut self, nick: &str, amount: usize) -> Result<Vec<String>> {
        let dealer = self.get_dealer_mut(nick).unwrap();

        let price = capacity_price(dealer.capacity, amount)?;

        dealer.money -= price;
        dealer.capacity += amount;

        let mut msg = PrivMsg::new();

        let msg = msg
            .text("you bought ")
            .color(IrcColor::Yellow)
            .text(&pretty_print_amount(amount))
            .reset()
            .text(" slots for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn show_all_dealers(&self, nick: &str) -> Result<Vec<String>> {
        let mut lines = vec![];

        lines.append(&mut hl_message(
            nick,
            &format!("There's currently {} dealers:", self.dealers.len()),
        ));

        for (dealer_name, dealer) in &self.dealers {
            lines.push(format!(
                "{} -> money: {}, hp: {:.2}, location: {}, capacity: {}",
                dealer_name,
                pretty_print_money(dealer.money),
                dealer.health,
                dealer.location,
                pretty_print_amount(dealer.capacity)
            ))
        }

        Ok(lines)
    }

    pub fn fast_forward(&mut self) -> Result<Vec<String>> {
        self.new_day()
    }

    pub fn save_game(&mut self, nick: &str) -> Result<Vec<String>> {
        let save = DrugWarsSave::from(self);

        if let Err(e) = save.save() {
            return Ok(hl_error(nick, &format!("An error occured: {:?}", e)));
        };

        Ok(hl_message(nick, "game saved"))
    }

    pub fn _attack(
        &mut self,
        nick: &str,
        target_nick: &str,
        weapon_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_mut(nick)?;
        dealer.has_attacked = true;

        let (weapon_name, item) = self.get_matching::<Item>(&weapon_str)?;
        let weapon_name = weapon_name.clone();

        let ItemKind::Weapon(weapon) = &item.kind else {
            return Err(Error::ItemNotWeapon(weapon_name.to_owned()));
        };

        let target_dealer = self.get_dealer(target_nick)?;

        let armor = target_dealer.get_best_armor(self);
        let damage = calc_damage(&mut self.rng.clone(), weapon, &armor);

        if weapon.ammo.is_some() {
            let ammo = weapon.ammo.as_ref().unwrap().clone();
            let dealer = self.get_dealer_mut(nick)?;
            dealer.sub_local::<Item>(&ammo, 1);
        }

        if self.rng.gen_bool(1. / 2.) {
            let mut msg = PrivMsg::new();
            let msg = msg.color(IrcColor::LightBlue).text("you missed!").get();

            return Ok(hl_message(nick, msg));
        }

        if armor.is_some() {
            let target_dealer = self.get_dealer_mut(target_nick)?;
            target_dealer.sub_local::<Item>(&armor.clone().unwrap().0, 1);
        }

        let target_dealer = self.get_dealer_mut(target_nick)?;

        target_dealer.health -= damage;

        let mut lines = vec![];

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("you hit ")
            .color(IrcColor::Yellow)
            .text(target_nick)
            .reset()
            .text(" with your ")
            .color(IrcColor::Yellow)
            .text(&weapon_name)
            .reset()
            .text(" and did ")
            .color(IrcColor::LightBlue)
            .text(&format!("{:.2}", damage))
            .reset()
            .text(" damage!")
            .get();

        lines.append(&mut hl_message(nick, msg));

        if self.rng.gen_bool(1. / 10.) {
            let mut msg = PrivMsg::new();
            let msg = msg
                .text("oh dear! You broke your ")
                .color(IrcColor::Yellow)
                .text(&weapon_name)
                .reset()
                .text("!")
                .get();

            lines.append(&mut hl_message(nick, msg));
            let dealer = self.get_dealer_mut(nick)?;
            dealer.sub_local::<Item>(&weapon_name, 1);
        }

        if armor.is_some() {
            let mut msg = PrivMsg::new();
            let msg = msg
                .text("you lost one ")
                .color(IrcColor::Yellow)
                .text(&armor.unwrap().0)
                .get();
            lines.append(&mut hl_message(target_nick, msg));
        }

        lines.append(&mut self.death(target_nick)?);

        Ok(lines)
    }

    pub fn _loot(&mut self, nick: &str, target_nick: &str) -> Result<Vec<String>> {
        let mut rng = rand::thread_rng();

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("you looted ")
            .color(IrcColor::Yellow)
            .text(target_nick)
            .reset()
            .text("!")
            .get();

        let mut lines = hl_message(nick, msg);

        let dealer = self.get_dealer(nick)?;
        let remaining_drug_cap = dealer.get_remaining_capacity_local::<Drug>();
        let remaining_item_cap = dealer.get_remaining_capacity_local::<Item>();

        let target_dealer = self.get_dealer_mut(target_nick)?;
        target_dealer.looters.insert(nick.to_owned());

        let money_taken = rng.gen_range(0..=target_dealer.money);
        target_dealer.money -= money_taken;

        let looted_drug = target_dealer.get_random::<Drug>(&mut rng);
        let looted_item = target_dealer.get_random::<Item>(&mut rng);

        let mut looted_drug_amount = 0;
        let mut looted_item_amount = 0;

        if let Some((looted_drug_name, looted_drug_owned)) = &looted_drug {
            looted_drug_amount =
                rng.gen_range(0..=looted_drug_owned.amount.min(remaining_drug_cap));
            target_dealer.sub_local::<Drug>(&looted_drug_name, looted_drug_amount);
            let mut msg = PrivMsg::new();
            let msg = msg
                .text("you took ")
                .color(IrcColor::Yellow)
                .text(&format!(
                    "{} {}",
                    pretty_print_amount(looted_drug_amount),
                    looted_drug_name
                ))
                .get();

            lines.push(msg.to_owned());
        }

        if let Some((looted_item_name, looted_item_owned)) = &looted_item {
            looted_item_amount =
                rng.gen_range(0..=looted_item_owned.amount.min(remaining_item_cap));
            target_dealer.sub_local::<Item>(&looted_item_name, looted_item_amount);
            let mut msg = PrivMsg::new();
            let msg = msg
                .text("you took ")
                .color(IrcColor::Yellow)
                .text(&format!(
                    "{} {}",
                    pretty_print_amount(looted_item_amount),
                    looted_item_name
                ))
                .get();

            lines.push(msg.to_owned());
        }

        let dealer = self.get_dealer_mut(nick)?;
        dealer.money += money_taken;
        let mut msg = PrivMsg::new();
        let msg = msg
            .text("you took ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(money_taken))
            .get();

        lines.push(msg.to_owned());

        if let Some((looted_drug_name, _)) = &looted_drug {
            dealer.add_local::<Drug>(&looted_drug_name, looted_drug_amount, 0);
        }
        if let Some((looted_item_name, _)) = &looted_item {
            dealer.add_local::<Item>(&looted_item_name, looted_item_amount, 0);
        }

        Ok(lines)
    }

    fn death(&mut self, nick: &str) -> Result<Vec<String>> {
        let death_day = self.settings.current_day.clone();

        let dealer = self.get_dealer_mut(nick)?;

        let mut lines = vec![];
        if dealer.health <= 0. {
            let mut msg = PrivMsg::new();
            let msg = msg
                .text("you are ")
                .color(IrcColor::Red)
                .text("DEAD! ")
                .reset()
                .text("Anyone at ")
                .color(IrcColor::Purple)
                .text(&dealer.location)
                .reset()
                .text(" can loot you. You will be able to play again in one week.")
                .get();

            lines.append(&mut hl_message(nick, msg));
            dealer.health = 0.;
            dealer.status = DealerStatus::Dead(death_day);
        }

        Ok(lines)
    }
}
