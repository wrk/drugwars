use std::{collections::HashMap, f32::consts::PI, str};

use rand::{Rng, RngCore};

use crate::{
    dealer::Dealer,
    definitions::{Armor, Location, MarketElement, OwnedElement, Weapon},
    drug_wars::DrugWars,
    error::Result,
};

pub trait Matchable {
    fn get_matching_elements<'a>(
        drug_wars: &'a DrugWars,
        name: &'a str,
    ) -> Vec<(&'a String, &'a Self)>
    where
        Self: Sized;
}

pub trait DealerComponent {
    fn get_elements_at<'a>(
        dealer: &'a Dealer,
        location: &'a str,
    ) -> &'a HashMap<String, OwnedElement>;

    fn get_elements_at_mut<'a>(
        dealer: &'a mut Dealer,
        location: &'a str,
    ) -> &'a mut HashMap<String, OwnedElement>;

    fn get_market_at<'a>(location: &'a Location) -> &'a HashMap<String, MarketElement>;
    fn get_market_at_mut<'a>(location: &'a mut Location) -> &'a mut HashMap<String, MarketElement>;
}

pub fn truncate_string(original: &str, max: usize) -> String {
    assert!(max > 3);

    if original.irc_safe_len() <= max {
        return original.to_owned();
    }

    format!("{}...", &original[..(max - 3)])
}

pub fn pretty_print_amount(amount: usize) -> String {
    let pretty_amount = amount
        .to_string()
        .as_bytes()
        .rchunks(3)
        .rev()
        .map(str::from_utf8)
        .collect::<std::result::Result<Vec<&str>, _>>()
        .unwrap()
        .join(",");

    format!("{}", pretty_amount)
}

pub fn pretty_print_money(money: u128) -> String {
    let unit_money = money / 10000;
    let float_money = money as f64 / 10000.;
    let dec = (float_money.fract() * 100.).floor() as u32;

    let pretty_money = unit_money
        .to_string()
        .as_bytes()
        .rchunks(3)
        .rev()
        .map(str::from_utf8)
        .collect::<std::result::Result<Vec<&str>, _>>()
        .unwrap()
        .join(",");

    format!("${}.{:0>2}", pretty_money, dec)
}

pub fn get_flight_price(origin: &Location, other: &Location) -> u128 {
    let cur_lat = origin.lat * (PI / 180.);
    let cur_long = origin.long * (PI / 180.);

    let other_lat = other.lat * (PI / 180.);
    let other_long = other.long * (PI / 180.);

    let float_price = (cur_lat.sin() * other_lat.sin()
        + cur_lat.cos() * other_lat.cos() * (other_long - cur_long).cos())
    .acos()
        * 10000.;

    (float_price * 10000.) as u128
}

pub fn get_shipping_price(origin: &Location, other: &Location, amount: usize) -> u128 {
    let flight_price = get_flight_price(origin, other);

    let shipping_price = (flight_price / 80) * amount as u128;

    shipping_price
}

pub fn capacity_price(current_capacity: usize, to_add: usize) -> Result<u128> {
    let price: f64 = 1000. * 10000.;

    let paid = price + current_capacity as f64 * (20000000. * current_capacity as f64 / 1000.);
    let total = current_capacity as f64 + to_add as f64;
    let max = price + total * (20000000. * total as f64 / 1000.);
    let to_be_paid = max - paid;

    Ok(to_be_paid as u128)
}

pub fn hl_message(nick: &str, message: &str) -> Vec<String> {
    return vec![format!("{}: {}", nick, message)];
}

pub fn hl_error(nick: &str, message: &str) -> Vec<String> {
    return vec![format!("{}: {}", nick, message)];
}

pub fn get_system_output(nick: &str, val: Result<Vec<String>>) -> Vec<String> {
    match val {
        Ok(output) => output,
        Err(err) => vec![format!("{}: {}", nick, err)],
    }
}

pub fn column_renderer_single(
    width: usize,
    header: &str,
    content: Vec<Vec<String>>,
) -> Vec<String> {
    if content.len() == 0 {
        return vec![];
    }

    // first, get the max row

    let total_columns = content[0].len();
    let column_width = ((width - 4) as f32 / total_columns as f32).floor() as usize;

    let mut lines = vec![];

    lines.push(format!(
        "╭ {} {}╮",
        header,
        "─".repeat(width - header.irc_safe_len() - 4)
    ));

    for row in content {
        let mut column = String::new();

        for cell in row {
            let value = truncate_string(&cell, column_width);
            let spaces = " ".repeat(column_width - value.irc_safe_len());
            column += &format!("{}{}", value, spaces);
        }

        column += &" ".repeat(width - column.irc_safe_len() - 4);
        lines.push(format!("│ {} │", column));
    }

    lines.push(format!("╰{}╯", "─".repeat(width - 2)));

    lines
}

pub trait IrcSafeLen {
    fn irc_safe_len(&self) -> usize;
}
impl IrcSafeLen for String {
    fn irc_safe_len(&self) -> usize {
        self.replace("\x0300", "")
            .replace("\x0301", "")
            .replace("\x0302", "")
            .replace("\x0303", "")
            .replace("\x0304", "")
            .replace("\x0305", "")
            .replace("\x0306", "")
            .replace("\x0307", "")
            .replace("\x0308", "")
            .replace("\x0309", "")
            .replace("\x0310", "")
            .replace("\x0311", "")
            .replace("\x0312", "")
            .replace("\x0313", "")
            .replace("\x0314", "")
            .replace("\x0315", "")
            .chars()
            .filter(|c| !['\x02', '\x1d', '\x1f', '\x1e', '\x12', '\x0f'].contains(c))
            .count()
    }
}

impl IrcSafeLen for &str {
    fn irc_safe_len(&self) -> usize {
        self.replace("\x0300", "")
            .replace("\x0301", "")
            .replace("\x0302", "")
            .replace("\x0303", "")
            .replace("\x0304", "")
            .replace("\x0305", "")
            .replace("\x0306", "")
            .replace("\x0307", "")
            .replace("\x0308", "")
            .replace("\x0309", "")
            .replace("\x0310", "")
            .replace("\x0311", "")
            .replace("\x0312", "")
            .replace("\x0313", "")
            .replace("\x0314", "")
            .replace("\x0315", "")
            .chars()
            .filter(|c| !['\x02', '\x1d', '\x1f', '\x1e', '\x12', '\x0f'].contains(c))
            .count()
    }
}

pub fn calc_damage(rng: &mut dyn RngCore, weapon: &Weapon, armor: &Option<(String, Armor)>) -> f32 {
    let mut damage = rng.gen_range((weapon.damage / 3.)..=weapon.damage);

    if armor.is_some() {
        damage -=
            rng.gen_range((armor.as_ref().unwrap().1.block / 2.)..=armor.as_ref().unwrap().1.block);
    }

    damage.max(0.)
}
