use std::{
    collections::HashMap,
    fs::File,
    io::{Read, Write},
    str::FromStr,
    time::SystemTime,
};

use chrono::NaiveDate;
use rand::{rngs::StdRng, SeedableRng};
use serde::{Deserialize, Serialize};

use crate::{
    config::DrugWarsConfig,
    dealer::Dealer,
    definitions::{Armor, Drug, Item, ItemKind, Location, MessageKind, Settings, Shipment, Weapon},
    drug_wars::DrugWars,
};

#[derive(Serialize, Deserialize)]
pub struct DrugWarsSave {
    pub day_duration: u32,
    pub current_day: String,
    pub save_path: String,
    pub config_path: String,
    pub width: usize,
    pub timer: SystemTime,
    pub dealers: HashMap<String, Dealer>,
    pub locations: HashMap<String, Location>,
    pub flights: HashMap<String, String>,
    pub shipments: Vec<Shipment>,
    pub laundering_fees: f32,
}

impl From<&mut DrugWars> for DrugWarsSave {
    fn from(drug_wars: &mut DrugWars) -> Self {
        Self {
            day_duration: drug_wars.settings.day_duration,
            current_day: drug_wars
                .settings
                .current_day
                .format("%Y-%m-%d")
                .to_string(),
            save_path: drug_wars.settings.save_path.clone(),
            config_path: drug_wars.settings.config_path.clone(),
            width: drug_wars.settings.width,
            timer: drug_wars.timer,
            dealers: drug_wars.dealers.clone(),
            locations: drug_wars.locations.clone(),
            flights: drug_wars.flights.clone(),
            shipments: drug_wars.shipments.clone(),
            laundering_fees: drug_wars.laundering_fees,
        }
    }
}

impl Into<DrugWars> for DrugWarsSave {
    fn into(self) -> DrugWars {
        let config: DrugWarsConfig = DrugWarsConfig::from_file(&self.config_path).unwrap();

        let mut drugs = HashMap::default();
        let mut items = HashMap::default();
        let mut messages = HashMap::default();

        for drug in config.drugs {
            let name = drug.as_mapping().unwrap()["name"].as_str().unwrap();
            let price = drug.as_mapping().unwrap()["price"].as_f64().unwrap();
            drugs.insert(
                name.to_owned(),
                Drug {
                    nominal_price: (price * 10000.) as u128,
                },
            );
        }

        let weapons = config.items["weapons"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for weapon in weapons {
            let name = weapon["name"].as_str().unwrap();
            let price = weapon["price"].as_f64().unwrap();
            let damage = weapon["damage"].as_f64().unwrap() as f32;

            let mut ammo = None;

            if weapon.contains_key("ammo") {
                ammo = Some(weapon["ammo"].as_str().unwrap().to_owned())
            }

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Weapon(Weapon { ammo, damage }),
                },
            );
        }

        let ammos = config.items["ammos"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for ammo in ammos {
            let name = ammo["name"].as_str().unwrap();
            let price = ammo["price"].as_f64().unwrap();

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Ammo,
                },
            );
        }

        let armors = config.items["armors"]
            .as_sequence()
            .unwrap()
            .iter()
            .map(|value| value.as_mapping().unwrap())
            .collect::<Vec<_>>();

        for armor in armors {
            let name = armor["name"].as_str().unwrap();
            let price = armor["price"].as_f64().unwrap();
            let block = armor["block"].as_f64().unwrap() as f32;

            items.insert(
                name.to_owned(),
                Item {
                    nominal_price: (price * 10000.) as u128,
                    kind: ItemKind::Armor(Armor { block }),
                },
            );
        }

        // OH LOOK ! I'M FUCKING SLEEP DEPRIVATED !
        for (val_str, enum_variant) in [
            ("price_up", MessageKind::PriceUp),
            ("price_up_end", MessageKind::PriceUpEnd),
            ("price_down", MessageKind::PriceDown),
            ("price_down_end", MessageKind::PriceDownEnd),
        ] {
            let msgs = &config.messages[val_str].as_sequence().unwrap();
            for msg in *msgs {
                let message_vec = messages.entry(enum_variant).or_insert_with(|| vec![]);
                message_vec.push(msg.as_str().unwrap().to_owned());
            }
        }

        DrugWars {
            settings: Settings {
                day_duration: self.day_duration,
                current_day: NaiveDate::from_str(&self.current_day).unwrap(),
                save_path: self.save_path,
                config_path: self.config_path,
                width: self.width,
            },
            rng: StdRng::from_entropy(),
            timer: self.timer,
            dealers: self.dealers.clone(),
            locations: self.locations.clone(),
            flights: self.flights.clone(),
            shipments: self.shipments.clone(),
            drugs,
            items,
            messages,
            laundering_fees: self.laundering_fees,
        }
    }
}

impl DrugWarsSave {
    pub fn save(&self) -> std::io::Result<()> {
        let content_str = serde_yaml::to_string(self).unwrap();
        let mut content = content_str.as_bytes();

        let mut file = File::create(&self.save_path)?;

        file.write_all(&mut content)?;
        Ok(())
    }

    pub fn load(path: &str) -> std::io::Result<Self> {
        let mut file = File::open(path)?;
        let mut contents = String::new();
        file.read_to_string(&mut contents)?;

        let config: DrugWarsSave = serde_yaml::from_str(&contents).unwrap();
        Ok(config)
    }
}
