use irc::{format::IrcColor, privmsg::PrivMsg};

use crate::{
    dealer::Dealer,
    definitions::{Item, ItemKind, Location},
    drug_wars::DrugWars,
    error::{Error, Result},
    utils::{
        capacity_price, get_flight_price, get_shipping_price, hl_message, pretty_print_amount,
        pretty_print_money, DealerComponent, Matchable,
    },
};

impl DrugWars {
    pub fn register_dealer(&mut self, nick: &str) -> Result<Vec<String>> {
        if self.dealers.contains_key(nick) {
            return Err(Error::DealerAlreadyRegistered);
        }

        self.dealers.insert(nick.to_owned(), Dealer::random(self));

        let dealer = self.get_dealer(nick)?;
        let location = self.locations.get_mut(&dealer.location.clone()).unwrap();

        location.blokes.insert(nick.to_owned());

        let mut msg = PrivMsg::new();
        let msg = msg
            .color(IrcColor::Purple)
            .text("get rich or die tryin")
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn show_market(&self, nick: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;
        Ok(self.render_market(nick, dealer))
    }

    pub fn show_info(&self, nick: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;
        Ok(self.render_info(nick, dealer))
    }

    pub fn show_people(&self, nick: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer(nick)?;
        Ok(self.render_people(dealer))
    }

    pub fn show_date_time(&self) -> Result<Vec<String>> {
        Ok(self.render_time())
    }

    pub fn show_all_commands(&self) -> Result<Vec<String>> {
        Ok(self.render_command_list())
    }

    pub fn leaderboard(&self) -> Result<Vec<String>> {
        Ok(self.render_leaderboard())
    }

    pub fn show_admin_commands(&self) -> Result<Vec<String>> {
        Ok(self.render_admin_command_list())
    }

    pub fn buy<T: DealerComponent + Matchable>(
        &mut self,
        nick: &str,
        name_str: &str,
        amount_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (name, _) = self.get_matching::<T>(&name_str)?;
        let amount = self.get_buy_amount_of::<T>(dealer, name, amount_str)?;

        let location = self.locations.get(&dealer.location).unwrap();
        let market = location.get_market::<T>();

        if !market.contains_key(name) {
            return Err(Error::NoElementAtMarket(name.to_owned()));
        }

        if dealer.get_total_owned_local::<T>() + amount > dealer.capacity {
            return Err(Error::NotEnoughCapacity);
        }

        let elem_at_market = market.get(name).unwrap();

        if elem_at_market.supply < amount {
            return Err(Error::NotEnoughSupply(name.to_owned()));
        }

        let total_price = elem_at_market.price * amount as u128;

        if total_price > dealer.money {
            return Err(Error::NotEnoughMoney);
        }

        self._buy::<T>(nick, &name.clone(), amount, elem_at_market.price)
    }

    pub fn sell<T: DealerComponent + Matchable>(
        &mut self,
        nick: &str,
        name_str: &str,
        amount_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (name, _) = self.get_matching::<T>(&name_str)?;

        let amount = self.get_sell_amount_of::<T>(dealer, name, amount_str)?;
        let location = self.locations.get(&dealer.location).unwrap();
        let market = T::get_market_at(location);

        if !market.contains_key(name) {
            return Err(Error::NoElementAtMarket(name.to_owned()));
        }

        let elem_at_market = market.get(name).unwrap();
        if elem_at_market.demand < amount {
            return Err(Error::NotEnoughDemand(name.to_owned()));
        }

        let owned_local = dealer.get_owned_local::<T>();

        if !owned_local.contains_key(name) {
            return Err(Error::NoElementOwned(name.to_owned()));
        }

        let owned_element = owned_local.get(name).unwrap();

        if owned_element.amount < amount {
            return Err(Error::NotEnoughElementOwned(name.to_owned()));
        }

        self._sell::<T>(nick, &name.clone(), amount)
    }

    pub fn give_money(
        &mut self,
        nick: &str,
        amount_str: &str,
        bloke_nick: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let money: u128 = self.get_money_amount(dealer, amount_str)?;

        self.get_dealer(bloke_nick)?;
        self._give_money(nick, bloke_nick, money)
    }

    pub fn give<T: DealerComponent + Matchable>(
        &mut self,
        nick: &str,
        name_str: &str,
        amount_str: &str,
        bloke_nick: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let bloke = self.get_dealer(bloke_nick)?;

        if dealer.location != bloke.location {
            return Err(Error::BlokeNotHere(bloke_nick.to_owned()));
        }

        let (name, _) = self.get_matching::<T>(&name_str)?;

        let owned_element_local = dealer.get_owned_local::<T>();

        if !owned_element_local.contains_key(name) {
            return Err(Error::NoElementOwned(name.to_owned()));
        }

        let owned_element = owned_element_local.get(name).unwrap();

        let amount = self.get_give_amount_of::<T>(dealer, bloke, name, amount_str)?;

        if owned_element.amount < amount {
            return Err(Error::NotEnoughElementOwned(name.to_owned()));
        }

        if bloke.get_total_owned_local::<T>() + amount > bloke.capacity {
            return Err(Error::BlokeNotEnoughCapacity(bloke_nick.to_owned()));
        }

        self._give::<T>(nick, bloke_nick, &name.clone(), amount)
    }

    pub fn check_flight_prices(&self, nick: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;
        Ok(self.render_prices_from(&dealer.location))
    }

    pub fn fly_to(&mut self, nick: &str, destination_str: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (destination_name, destination) = self.get_matching(&destination_str)?;

        let current_location = self.locations.get(&dealer.location).unwrap();
        let price = get_flight_price(current_location, destination);

        if dealer.money < price {
            return Err(Error::NotEnoughMoney);
        }

        self._fly_to(nick, &destination_name.clone())
    }

    pub fn check_capacity_price(&mut self, nick: &str, amount_str: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let amount = self.get_amount_from_str(amount_str)?;
        let price = capacity_price(dealer.capacity, amount)?;

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("it will cost you ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .reset()
            .text(" to buy ")
            .color(IrcColor::Yellow)
            .text(&pretty_print_amount(amount))
            .reset()
            .text(" slots.")
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn check_shipping_price<T: DealerComponent + Matchable>(
        &mut self,
        nick: &str,
        name_str: &str,
        amount_str: &str,
        destination_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (name, _) = self.get_matching::<T>(&name_str)?;

        if !dealer.get_owned_local::<T>().contains_key(name) {
            return Err(Error::NoElementOwned(name.to_owned()));
        }

        let (destination_name, destination) = self.get_matching::<Location>(&destination_str)?;

        if dealer.location == *destination_name {
            return Err(Error::ShipCurrentLocation);
        }

        let amount = self.get_ship_amount_of::<T>(dealer, destination_name, name, amount_str)?;

        let target_remaining_capacity = dealer.get_remaining_capacity_at::<T>(destination_name);

        if target_remaining_capacity < amount {
            return Err(Error::NotEnoughCapacityAt(destination_name.to_owned()));
        }

        let dealer_location = self.locations.get(&dealer.location).unwrap();

        let price = get_shipping_price(dealer_location, destination, amount);

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("you'll have to pay ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(price))
            .reset()
            .text(" to ship ")
            .color(IrcColor::Yellow)
            .text(&format!("{} {}", pretty_print_amount(amount), name))
            .reset()
            .text(" to ")
            .color(IrcColor::Purple)
            .text(&destination_name)
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn ship<T: DealerComponent + Matchable>(
        &mut self,
        nick: &str,
        name_str: &str,
        amount_str: &str,
        destination_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (name, _) = self.get_matching::<T>(&name_str)?;

        if !dealer.get_owned_local::<T>().contains_key(name) {
            return Err(Error::NoElementOwned(name.to_owned()));
        }

        let (destination_name, destination) = self.get_matching::<Location>(&destination_str)?;

        if dealer.location == *destination_name {
            return Err(Error::ShipCurrentLocation);
        }

        let amount = self.get_ship_amount_of::<T>(dealer, &destination_name, name, amount_str)?;

        let owned_local = dealer.get_owned_local::<T>();

        if !owned_local.contains_key(name) {
            return Err(Error::NoElementOwned(name.to_owned()));
        }

        let element = owned_local.get(name).unwrap();

        if element.amount < amount {
            return Err(Error::NotEnoughElementOwned(name.to_owned()));
        }

        let target_remaining_capacity = dealer.get_remaining_capacity_at::<T>(destination_name);

        if target_remaining_capacity < amount {
            return Err(Error::NotEnoughCapacityAt(destination_name.to_owned()));
        }

        let dealer_location = self.locations.get(&dealer.location).unwrap();

        let price = get_shipping_price(dealer_location, destination, amount);

        if dealer.money < price {
            return Err(Error::NotEnoughMoney);
        }

        self._ship::<T>(nick, &name.clone(), amount, &destination_name.clone())
    }

    pub fn buy_capacity(&mut self, nick: &str, amount_str: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let amount = self.get_amount_from_str(amount_str)?;

        let price = capacity_price(dealer.capacity, amount)?;

        if dealer.money < price {
            return Err(Error::NotEnoughMoney);
        }

        self._buy_capacity(nick, amount)
    }

    pub fn attack(
        &mut self,
        nick: &str,
        target_nick: &str,
        weapon_str: &str,
    ) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        if dealer.has_attacked {
            return Err(Error::AlreadyAttacked);
        }

        let Ok(target_dealer) = self.get_dealer(target_nick) else {
            return Err(Error::DealerNotFound(target_nick.to_owned()));
        };

        if !target_dealer.available() {
            return Err(Error::CantAttackDealer(
                target_nick.to_owned(),
                target_dealer.status.clone(),
            ));
        }

        if dealer.location != target_dealer.location {
            return Err(Error::NotSameLocation(target_nick.to_owned()));
        }

        let (weapon_name, weapon) = self.get_matching::<Item>(weapon_str)?;

        let ItemKind::Weapon(w) = &weapon.kind else {
            return Err(Error::ItemNotWeapon(weapon_name.to_owned()));
        };

        dealer.can_use_weapon(weapon_name, w.ammo.as_deref())?;

        self._attack(nick, target_nick, weapon_str)
    }

    pub fn heal(&mut self, nick: &str) -> Result<Vec<String>> {
        self.get_dealer_available(nick)?;

        let dealer = self.get_dealer_mut(nick)?;

        if dealer.health == 100. {
            return Err(Error::AlreadyFullHealth);
        }

        dealer.health = 100.;
        let cost = dealer.money / 3;

        dealer.money -= cost;

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("you restored all your health for ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(cost))
            .get();

        Ok(hl_message(nick, msg))
    }

    pub fn loot(&mut self, nick: &str, target_nick: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let Ok(target_dealer) = self.get_dealer(target_nick) else {
            return Err(Error::DealerNotFound(target_nick.to_owned()));
        };

        if !target_dealer.is_dead() {
            return Err(Error::DealerNotDead(target_nick.to_owned()));
        }

        if dealer.location != target_dealer.location {
            return Err(Error::NotSameLocation(target_nick.to_owned()));
        }

        if target_dealer.looters.contains(nick) {
            return Err(Error::AlreadyLooted(target_nick.to_owned()));
        }

        self._loot(nick, target_nick)
    }

    pub fn launder(&mut self, nick: &str, amount_str: &str) -> Result<Vec<String>> {
        let dealer = self.get_dealer_available(nick)?;

        let (laundered, amount) = self.get_laundering_amount(dealer, amount_str)?;

        let dealer = self.get_dealer_mut(nick)?;
        dealer.money -= amount;
        dealer.laundered_money += laundered;

        let mut msg = PrivMsg::new();
        let msg = msg
            .text("You laundered ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(amount))
            .reset()
            .text(" into ")
            .color(IrcColor::Green)
            .text(&pretty_print_money(laundered))
            .reset()
            .text(" with a fee of ")
            .color(IrcColor::Yellow)
            .text(&format!("{:.2}%", self.laundering_fees * 100.))
            .get();

        Ok(hl_message(nick, msg))
    }
}
