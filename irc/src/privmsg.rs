use crate::format::{IrcColor, IrcFormat};
pub struct PrivMsg(String);

impl PrivMsg {
    pub fn new() -> Self {
        Self(String::new())
    }

    pub fn color(&mut self, color: IrcColor) -> &mut Self {
        self.0 += &IrcFormat::Color.to_string();
        self.0 += &color.to_string();
        self
    }

    pub fn format(&mut self, format: IrcFormat) -> &mut Self {
        self.0 += &format.to_string();
        self
    }

    pub fn reset(&mut self) -> &mut Self {
        self.0 += &IrcFormat::Plain.to_string();
        self
    }

    pub fn text(&mut self, text: &str) -> &mut Self {
        self.0 += text;
        self
    }

    pub fn get(&self) -> &str {
        &self.0
    }
}
