#[derive(Clone, Copy)]
pub enum IrcFormat {
    Bold,
    Italics,
    Underline,
    Strikethrough,
    Reverse,
    Color,
    Plain,
}

impl std::fmt::Display for IrcFormat {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            IrcFormat::Bold => write!(f, "\x02"),
            IrcFormat::Italics => write!(f, "\x1d"),
            IrcFormat::Underline => write!(f, "\x1f"),
            IrcFormat::Strikethrough => write!(f, "\x1e"),
            IrcFormat::Reverse => write!(f, "\x12"),
            IrcFormat::Color => write!(f, "\x03"),
            IrcFormat::Plain => write!(f, "\x0f"),
        }
    }
}

#[derive(Clone, Copy)]
pub enum IrcColor {
    White,
    Black,
    Blue,
    Green,
    Red,
    Brown,
    Purple,
    Orange,
    Yellow,
    LightGreen,
    Teal,
    Cyan,
    LightBlue,
    Magenta,
    Gray,
    LightGray,
}

impl std::fmt::Display for IrcColor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            IrcColor::White => write!(f, "00"),
            IrcColor::Black => write!(f, "01"),
            IrcColor::Blue => write!(f, "02"),
            IrcColor::Green => write!(f, "03"),
            IrcColor::Red => write!(f, "04"),
            IrcColor::Brown => write!(f, "05"),
            IrcColor::Purple => write!(f, "06"),
            IrcColor::Orange => write!(f, "07"),
            IrcColor::Yellow => write!(f, "08"),
            IrcColor::LightGreen => write!(f, "09"),
            IrcColor::Teal => write!(f, "10"),
            IrcColor::Cyan => write!(f, "11"),
            IrcColor::LightBlue => write!(f, "12"),
            IrcColor::Magenta => write!(f, "13"),
            IrcColor::Gray => write!(f, "14"),
            IrcColor::LightGray => write!(f, "15"),
        }
    }
}
